# GBX Protocol Description
_gbx_ is a binary serialization format for data handling within GSS (GRID Software Suite) and related software. The _gbx_ format defines the binary representation of several reports (sometimes referred to as _messages_). _gbx_ supersedes the _gbx4_ protocol specification.

## Table of Contents
1. [Conventions](#conventions)
2. [Numeric Data Types](#numeric-data-types)
3. [GBX File/Stream Structure](#gbx-file-stream-structure)
4. [GBX Report Structure](#gbx-report-structure)
    - [Header](#header)
    - [Payload](#payload)
    - [Footer](#footer)
    - [Sample Report](#sample-report)
5. [Deserialization techniques and recommendations](#deserialization-techniques-and-recommendations)
6. [Example Program for Processing GBX Data](#example-program-for-processing-gbx-data)
7. [GBX Stream Structure](#gbx-stream-structure)
	- [The Epoch](#the-epoch)
	- [IMU and IMU Configuration Reports](#imu-and-imu-configuration-reports)
8. [The Report Types](#the-report-types)

## Conventions
The following conventions shall apply throughout this document:

- All numerals with prefix '0x' are in base-16 (hexadecimal) notation.
- All numerals without a prefix are in base-10 (decimal) notation.
- value < m > represents bit _m_ in value, where bit 0 is the least significant bit.
- value < m : n > represents the (_m_ - _n_ + 1)-bit bitfield from bit _m_ down to bit _n_.

## Numeric Data Types
All multi-byte fields are stored in **little-endian byte order**. All signed integers are stored in **twos-complement** representation.

Type      | Description
----      | -----------
u8        | unsigned 8-bit integer
s8        | signed 8-bit integer
u16       | unsigned 16-bit integer
s16       | signed 16-bit integer
u32       | unsigned 32-bit integer
s32       | signed 32-bit integer
u64       | unsigned 64-bit integer
s64       | signed 64-bit integer
f32       | single-precision IEEE-754 floating point value
f64       | double-precision IEEE-754 floating point value
b<b>N</b> | **N**-bit bitfield

## GBX File/Stream Structure
A properly structured _gbx_ file is simply a file which contains one or several _gbx reports_ (and nothing else). For example, a _gbx_ file which contains 2 reports, each being 10 bytes in length, is exactly 20 bytes in length. This means that there are not any seperate headers at the beginning of a _gbx_ file which might be used, for example, to specify the origins of the file. As such, it is recommended that you track the origins of your _gbx_ files separately (perhaps with a similarly named .txt file) to stay organized. The recommended file extension for a _gbx_ file is ``.gbx``.

## GBX Report Structure
_gbx_ reports are composed of a header, payload, and footer (in that order). As will be seen, the header has a fixed size of 8 bytes, the payload varies with report type, and the footer has a fixed size of 2 bytes. A payload size of 0 is valid and, as an example, such a payload would be serialized into a _gbx_ report totaling 10 bytes in length.

### Header
Offset | Type | Name/Value | Description
------ | ---- | ---------- | -----------
0      | u8   | 0x55       | First synchronization byte
1      | u8   | 0x54       | Second synchronization byte
2      | u8   | reportType | Report type
3      | u8   | streamId   | Stream identifier
4      | u32  | reportSize | Report payload size

The **fixed size of the header is 8 bytes**.

#### Synchronization Bytes
Protocol-specific values which should always be verified during deserialization. Earlier versions of the _gbx_ format are similar in report structure but had different values for synchronization bytes. This change was deliberate to prevent confusion.

#### Report Type (``reportType``)
Specifies the type of report; that is, the nature and structure of data to be found in the payload. A table of defined types is found [here](#index).

#### Stream Identifier (``streamId``)
Allows for multiple logical streams to be multiplexed into a single _gbx_ report stream. Each specific logical stream (e.g., a particular GPS receiver) shall be assigned a unique ``streamId`` value. For a _gbx_ report stream containing only one logical stream, this value will normally be 0. In the case where a second logical stream is included (e.g., a reference GNSS receiver for RTK), this second stream should be assigned a value of 1.

Using any other values with the standard set of GSS tools may result in ignoring the stream. At worst, it will result in undefined behavior.

#### Report Size (``reportSize``)
Specifies the size of the payload (in bytes). This value __does not__ include the additional length of the header and footer.

### Payload
Structure varies dependent on the specified ``reportType``. The size of the payload is specified (in bytes) by the ``reportSize`` field of the header. The payload of every report type is actually a Google Protobuf 3 binary buffer. The developer's guide for ``proto3`` is available [here](https://developers.google.com/protocol-buffers/docs/proto3). Message definition files can be found in the ``gss`` repository under the ``gss/gbxframework/messages`` directory.

### Footer
Offset | Type | Name/Value | Description
------ | ---- | ---------- | -----------
0      | u16  | checksum   | Fletcher-16 checksum

The **fixed size of the footer is 2 bytes**. During report deserialization, the _Fletcher-16 checksum_ should be calculated locally for the received header and payload. This calculated checksum should then be compared against the received checksum. If the calculated and received checksums do not match then the report shall be considered corrupt and discarded. The _Fletcher-16_ checksum value is for the **header and payload** and can be calculated with the following algorithm (C implementation):
~~~c
u16 fletcher16(const u8* data, u32 bytes) {
  u16 sum1 = 0xff, sum2 = 0xff;
  u32 tlen;
  while (bytes) {
    tlen = ((bytes >= 20) ? 20 : bytes);
    bytes -= tlen;
    do {
      sum2 += sum1 += *data++;
      tlen--;
    } while (tlen);
    sum1 = (sum1 & 0xff) + (sum1 >> 8);
    sum2 = (sum2 & 0xff) + (sum2 >> 8);
  }
  /* Second reduction step to reduce sums to 8 bits */
  sum1 = (sum1 & 0xff) + (sum1 >> 8);
  sum2 = (sum2 & 0xff) + (sum2 >> 8);
  return (sum2 << 8) | sum1;
}
~~~

**NOTE**: Variations of the _Fletcher-16_ checksum implementation may provide different checksum values for a particular input. If using different implementations (by algorithm or language), it is recommended that you compare their output against the provided algorithm.

### Sample Report
A valid and complete _gbx_ report: ``55 54 14 00 03 00 00 00 08 82 01 4C DD`` (hexadecimal byte values, left byte first)

The first 8 bytes represent the [header](#header) and communicate the following:

- This is a _gbx_ report (``0x55 0x54``),
- of report type ``CODA`` (by [this](#index) table),
- multiplexed on streamId 0,
- with a payload that is 3 bytes in length (note Little-Endian byte ordering of a u32)

The 3 byte payload contains: ``0x08 0x82 0x01``

And the Fletcher-16 checksum (the 2 byte [footer](#footer)) of the first 11 bytes is ``0x4C 0xDD``, the Little-Endian representation of the value ``0xDD4C``.

## GBX Stream Structure
With few exceptions, each individual report is entirely self-contained; that is, it contains all relevant information required to use all other information contained in the report. Where reports are found in the stream relative to other reports is generally irrelevant (other than to infer relative timing). Reports can generally be encountered in any order. This is the rule. The exceptions are to follow.

### The Epoch
The _epoch_ represents a set of reports whose mutual information is applicable to the same instant in time (the _epoch_). The _epoch_ is the periodic output (say, 5Hz) of the GNSS receiver, PpRx. Every _epoch_ begins with a report of type ``OBSERVABLES_MEASUREMENT_TIME `` and ends with a report of type ``CODA``. Any ``OBSERVABLES_MEASUREMENT_TIME `` (or ``CODA``) report encountered without an associated ``CODA`` (or ``OBSERVABLES_MEASUREMENT_TIME ``) shall be considered to represent a corrupt stream or incomplete _epoch_. The following report types represent the _epoch_ produced by PpRx:

1. ``GNSS_OBSERVABLES``
1. ``STANDARD_NAVIGATION_SOLUTION``
1. ``TRANSMITTER_INFO``
1. ``IQ_METADATA``
1. ``IONOSPHERE``
1. ``SCINTILLATION_PARAMETERS``

Note that other applications (e.g., another PpRx instance or an RTK application) operating on a PpRx-produced _gbx_ stream may add other reports to the epoch, or may remove PpRx-emplaced reports.

The _epoch_ has two primary purposes:
1. The first purpose is to allow proper interpretation of individually _incomplete_ reports (e.g., due to not storing an applicable timestamp). Any timestamp associated with one _epoch_-associated report is applicable to all _epoch_-associated reports.
1. The second purpose is to facilitate robust, low-latency stream processing. The end of the _epoch_ (the ``CODA`` report) serves as an explicit signal that no other reports associated with this epoch will be received in the future. Without the benefit of this explicit signal, a processing node could only infer the completion of one epoch by the beginning of the next epoch (with resultant substantial latency).

Not all _epoch_-associated report types may be present in any given epoch; in fact, a valid epoch may contain no _epoch_-associated report types.  Some _epoch_-associated report types may be repeated multiple times.

If any other report types have an associated timestamp which is exactly coincident to that of the _epoch_ (by design and not mere coincidence) then they shall also be members of that _epoch_. For example, the result of any measurement update using GNSS observables of the _epoch_ is, by design, coincident with the _epoch_ and is a member of that _epoch_.

A stream is considered ill-formed if any _epoch_-associated report is encountered outside of an associated pairing of ``OBSERVABLES_MEASUREMENT_TIME`` and ``CODA`` reports; undefined behavior may result. It is not required that the recipient ignore these _misplaced reports_. Rather, the producer shall assume that they will be ignored.

Any and all other report types may be encountered between a pairing of ``OBSERVABLES_MEASUREMENT_TIME`` and ``CODA`` reports. This is allowable but circumstantial. These other report types are not associated with the epoch.

### IMU and IMU Configuration Reports
``IMU`` and ``IMU_CONFIG``  are not entirely self-contained and independent. The ``IMU`` report contains a partial timestamp (the lower 32-bits of a 64-bit value). The ``IMU_CONFIG`` report is periodically published and contains (among other things) a full 64-bit timestamp. The upper 32-bits of the full 64-bit timestamp from the ``IMU_CONFIG`` report should be used to disambiguate and recover the full 64-bit timestamp associated with each ``IMU`` report. If the need arises to use this fact and further clarification is required, contact the developers.

## Deserialization techniques and recommendations
A valid _gbx_ report can be identified in a byte-stream through the following process:
1. Locate the first occurrence of the two synchronization bytes (``0x55 0x54``).
1. Assuming this sequence marks the beginning of a valid _gbx_ report header, extract the payload size. Use the payload size to extract the footer checksum.
1. Calculate the checksum of the received header and payload and compare against the received checksum found in the footer.
  - If the checksums compare equal then it can be assumed that a valid _gbx_ report has been found.
  - If the checksums do not compare equal then either misalignment or corruption has occurred. In either case, the payload size is suspect and should not be used to attempt to skip forward to the next report. Instead, the next occurrence of ``0x55 0x54`` should be located and the process repeated.

Undesired reports can be ignored based on report type and/or stream identifier. In these cases, it is not necessary to deserialize their payloads. However, it is recommended to still calculate and compare checksums on ignored reports.

Keep in mind that filtering reports from a stream may impact proper operation of GSS tools. Importantly, if either ``OBSERVABLES_MEASUREMENT_TIME`` and/or ``CODA`` reports are filtered then _the epoch_ structure of the _gbx4_ stream will be damaged and all _epoch_-associated reports will be negatively impacted (and potentially rendered useless). In short, it is best to only filter report types from a stream if you know exactly what you are doing.

## Example Program for Processing GBX Data
An example program called _gbxanalyzer_ has been created to illustrate how to
open a _gbx_ file/stream and extract data from _gbx_ reports. This program gets built automatically as part of the overall _gss_ build process,
and gets installed automatically upon execution of `make install`.

Suppose execution of _pprx_ has produced a _gbx_ output file called `dataout.gbx`. Then executing `gbxanalyzer --in dataout.gbx` will process `dataout.gbx` and output an ASCII file `contentReport.log` listing
all the report types present in `dataout.gbx`.  The code for _gbxanalyzer_ is contained in a single source file, [gbxanalyzer.cpp](https://gitlab.com/radionavlab/core/gss/blob/master/shared/binflate/src/gbxanalyzer.cpp).
Notes within the source file offer explanations of the various steps involved in the simple example program.

## The Report Types
Given that the payload sections of each report are serialized by the Google Protobuf 3 library, their binary representations cannot practically be detailed here. The [index](#index) of reports can be found in ``gss/gbxframework/include/reportframework.h`` in ``enum Report::Type``. Report implementations are defined in ``gss/gbxframework/include/reportdefinitions.h`` (as C++ classes with comments) and ``gss/gbxframework/messages/*.proto`` (``proto3`` payload definitions).

### Index
The presence of report types in this list does not necessarily imply software capabilities that are available for licensing (or that even exist) at this time. Certain report types might be implemented for future-use or not even fully implemented. All report type values not included in this list shall be considered reserved.  Cross-check this list with [reporttype.inc](https://gitlab.com/radionavlab/core/gss/-/blob/master/gbxframework/include/reporttype.inc?ref_type=heads) to ensure completeness and accuracy.

Name                           | Report Type
----                           | -----------
DUMMY_REPORT                   | 0x00
IQ                             | 0x01
GNSS_OBSERVABLES               | 0x02
OBSERVABLES_MEASUREMENT_TIME   | 0x03
ESTIMATOR_INNOVATIONS          | 0x04
ESTIMATOR_STATE                | 0x05
IMU                            | 0x06
IMU_CONFIG                     | 0x07
TRANSMITTER_INFO               | 0x08
IQ_METADATA                    | 0x09
SCINTILLATION_PARAMETERS       | 0x0A
IONOSPHERE                     | 0x0B
DIAGNOSTIC_MESSAGE             | 0x0C
ANTENNA_PCV                    | 0x0D
POSE_AND_TWIST                 | 0x0E
STANDARD_NAVIGATION_SOLUTION   | 0x0F
TRIGGER_TIME                   | 0x10
EPHEMERIS                      | 0x11
ALMANAC                        | 0x12
BITCONTAINER                   | 0x13
CODA                           | 0x14
SPECTRUM                       | 0x15
INFO                           | 0x16
STATUS                         | 0x17
ATTITUDE_2D                    | 0x18
ATTITUDE_3D                    | 0x19
SINGLE_BASELINE_RTK            | 0x1A
MULTI_BASELINE_RTK_ATTITUDE_2D | 0x1B
MULTI_BASELINE_RTK_ATTITUDE_3D | 0x1C
RADAR                          | 0x1D
RADAR_CONFIG                   | 0x1E
TIME_CONVERSION                | 0x1F
EPHEMERIS_PARAMETERS           | 0x20
ATMOSPHERIC_PARAMETERS         | 0x21
DIFFERENTIAL_CODE_BIAS         | 0x22
IMAGE                          | 0x23
MEASUREMENTS                   | 0x24
MEASUREMENTS_BATCH             | 0x25
DIFFERENTIAL_CORRECTIONS       | 0x26
COMMAND                        | 0x27
COMMAND_RESPONSE               | 0x28
