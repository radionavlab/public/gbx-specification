# gbx-specification

The GRID Binary Exchange (GBX) format is described [here](https://gitlab.com/radionavlab/public/gbx-specification/-/blob/master/gbx/protocol-description-gbx.md).

.proto files for each message type are found in [messages](https://gitlab.com/radionavlab/public/gbx-specification/-/tree/master/messages).

Further definitions of time scales, observables, flags, etc., are found in [log_file_definitions](https://gitlab.com/radionavlab/public/gbx-specification/-/tree/master/log_file_definitions).

An example properly-formatted GBX file and corresponding contents description are found in [example](https://gitlab.com/radionavlab/public/gbx-specification/-/tree/master/example?ref_type=heads).

## License
The contents of this repository are licensed under the [MIT license](https://opensource.org/license/mit/).  

Copyright 2024 The GRID Software Project

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


