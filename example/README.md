# Example GBX file

pprx.gbx serves as an example of a properly-formatted GBX file from a rover (as
opposed to reference) receiver with two antennas.  The data were produced by
processing raw front-end samples and IMU data from the [TEX-CUP
dataset](https://radionavlab.ae.utexas.edu/texcup-desc/) rover vehicle for May
12, 2019.

The file contentReport.log shows the sequence of reports embedded in pprx.gbx.
An example _epoch_ from this file appears as follows:

```
26812     63306.600           0    OBSERVABLES_MEASUREMENT_TIME    
26813     63306.600           0    GNSS_OBSERVABLES                
26814     63306.600           0    GNSS_OBSERVABLES                
26815     63306.600           0    GNSS_OBSERVABLES                
26816     63306.600           0    GNSS_OBSERVABLES                
26817     63306.600           0    GNSS_OBSERVABLES                
26818     63306.600           0    GNSS_OBSERVABLES                
26819     63306.600           0    GNSS_OBSERVABLES                
26820     63306.600           0    GNSS_OBSERVABLES                
26821     63306.600           0    STANDARD_NAVIGATION_SOLUTION    
26822     63306.600           0    STANDARD_NAVIGATION_SOLUTION    
26823     63306.600           0    TRANSMITTER_INFO                
26824     63306.600           0    MEASUREMENTS                    
26825     63306.600           0    MEASUREMENTS                    
26826     63306.600           0    MEASUREMENTS                    
26827     63306.600           0    MEASUREMENTS                    
26828     63306.600           0    MEASUREMENTS                    
26829     63306.600           0    MEASUREMENTS                    
26830     63306.600           0    MEASUREMENTS                    
26831     63306.600           0    MEASUREMENTS                    
26832     63306.600           0    TRIGGER_TIME                    
26833     63306.600           0    MEASUREMENTS                    
26834     63306.600           0    MEASUREMENTS                    
26835     63306.600           0    MEASUREMENTS                    
26836     63306.600           0    MEASUREMENTS                    
26837     63306.600           0    MEASUREMENTS                    
26838     63306.600           0    MEASUREMENTS                    
26839     63306.600           0    MEASUREMENTS                    
26840     63306.600           0    MEASUREMENTS                    
26841     63306.600           0    MEASUREMENTS                    
26842     63306.600           0    MEASUREMENTS                    
26843     63306.600           0    TRIGGER_TIME                    
26844     63306.600           0    MEASUREMENTS                    
26845     63306.600           0    MEASUREMENTS                    
26846     63306.600           0    IONOSPHERE                      
26847     63306.600           0    IONOSPHERE                      
26848     63306.600           0    IONOSPHERE                      
26849     63306.600           0    IONOSPHERE                      
26850     63306.600           0    IONOSPHERE                      
26851     63306.600           0    IONOSPHERE                      
26852     63306.600           0    IONOSPHERE                      
26853     63306.600           0    CODA
```    

Note that the MEASUREMENTS reports (containing IMU data in this case), happen
to fall within the bookends of the epoch (the OBSERVABLES_MEASUREMENT_TIME and
CODA reports) but are self-contained reports that may also appear outside an
epoch.  Likewise, TRIGGER_TIME reports can reside outside an epoch.
